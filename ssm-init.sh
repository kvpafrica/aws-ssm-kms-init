#!/usr/bin/env bash
if [ -z "$AWS_DEFAULT_REGION" ]; then export AWS_DEFAULT_REGION=$(curl -sq http://169.254.169.254/latest/dynamic/instance-identity/document|grep region|awk -F\" '{print $4}'); fi
VARS=`printenv`
WITH_DECRYPTION="--with-decryption"


for KVPAIR in $VARS; do
	PARTS=(`echo $KVPAIR | sed "s/=/ /" | tr ' ' "\n"` )
	if  [[ ${PARTS[1]} == ssm:* ]] ;
	then
		echo "Found SSM parameter: ${PARTS[0]} => ${PARTS[1]}"
		SSM_PARAM_NAME=${PARTS[1]/ssm:/}
		if  [[ ${SSM_PARAM_NAME} == nodecrypt:* ]] ;
		then
		   export SSM_PARAM_NAME=${SSM_PARAM_NAME/nodecrypt:/}
		   WITH_DECRYPTION="--no-with-decryption"
		fi
		export ${PARTS[0]}=`aws ssm get-parameter --name=$SSM_PARAM_NAME --query="Parameter.Value" --output 'text' $WITH_DECRYPTION`
	fi
done
