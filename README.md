# AWS SSM & KMS Initialization

This is a collection of bash files that can be used to ensure that your Docker container can use AWS KMS secrets and AWS KMS credentials. 

Please note that these libraries assume that you are running on AWS, and that your instance has AWS instance roles. Using these containers outside AWS will require you passing in environment variables for [AWS credentials](https://docs.aws.amazon.com/cli/latest/userguide/cli-environment.html) during initialization of container.

## About Scripts



## Steps to Using Library

1. Install AWS CLI by running ./aws-install.sh script.
1. Add KMS initialization to your bash initialization (if you're using KMS)
1. Add the SSM library to the container (if using SSM)

## AWS Installer

The AWS installer script is `aws-install.sh`, it contains commands for installing AWS CLI on the Docker container. You can run by calling `./aws-install.sh`

NB: This script works for Debian based containers.

### Including AWS CLI with Container

Add the following lines to your docker file:

```
ADD aws-install.sh /aws-install.sh
RUN chmod +x /aws-install.sh
RUN /aws-install.sh
```


## AWS KMS Initializer (kms-init.sh)

The KMS initializer, decrypts environment variables that are encrypted with AWS KMS and sets the variable as the decrypted value. This initializer should be loaded before application is started.

**How it works:**

This script expects that all environment variable starting with ENC_ are encrypted with KMS, so as an example, given environment variable with name: `ENC_DB_PASSWORD`, this script will decrypt the value with KMS and store decrypted value in `DB_PASSWORD` environment variable.

### Adding AWS KMS Initializer to Container

Add the following lines to your Dockerfile:

```
ADD kms-init.sh /kms-init.sh
RUN chmod +x /kms-init.sh
RUN echo "source /kms-init.sh" >> /root/.bashrc #i.e. initialize when bash loads

```

## AWS SSM Initializer (ssm-init.sh)

The SSM initializer will retrieve all environment variable whose values starts with the `ssm:`  or `ssm:nodecrypt:` prefix. This script replaces the value of the original environment variable with a new variable that has the retrieved SSM variable. 

By default, this script attempts to decrypt an environment variable, if you don't want this behaviour for a SecureString SSM parameter, then use the `ssm:nodecrypt:` prefix instead.

For example the following variable: `DB_PASSWORD=ssm:service.db.password` will be transformed to the decrypted value for `service.db.password` if encryptedor just the plain value if no encryption.


### Adding AWS SSM Initializer to Container 

Add the following lines to your container 

```
ADD ssm-init.sh /ssm-init.sh
RUN chmod +x /ssm-init.sh
RUN echo "source /ssm-init.sh.sh" >> /root/.bashrc #i.e. initialize when bash loads

```



## Example: Docker Image with All Scripts

```
ADD https://bitbucket.org/kvpafrica/aws-ssm-kms-init/get/master.tar.gz /scripts.tar.gz
RUN tar -xzf /scripts.tar.gz && \
	mv kvpafrica-aws-ssm-kms-init-*/*.sh / && \
	chmod +x /aws-install.sh /kms-init.sh /ssm-init.sh

RUN /aws-install.sh \
		&& echo "source /kms-init.sh" >> /root/.bashrc \
		&& echo "source /ssm-init.sh" >> /root/.bashrc

```

Please note that your entrypoint needs to explicitly call these scripts, see example below

```
source /root/.bashrc
nginx -d
```