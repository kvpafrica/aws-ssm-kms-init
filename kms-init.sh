#!/usr/bin/env bash
if [ -z "$AWS_DEFAULT_REGION" ]; then export AWS_DEFAULT_REGION=$(curl -sq http://169.254.169.254/latest/dynamic/instance-identity/document|grep region|awk -F\" '{print $4}'); fi
VARS=`printenv | grep "^ENC_"`

for KVPAIR in $VARS; do
	PARTS=(`echo $KVPAIR | sed "s/=/ /" | tr ' ' "\n"` )
	tmp_file=`mktemp`
	NEW_NAME=${PARTS[0]/ENC_/}
	echo ${PARTS[1]} | base64 --decode > $tmp_file
	NVAL=`aws kms decrypt --ciphertext-blob fileb://$tmp_file  --output text --query Plaintext | base64 --decode`
	rm $tmp_file
	export $NEW_NAME=$NVAL
done